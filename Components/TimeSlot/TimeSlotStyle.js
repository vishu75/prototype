//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get('window');

const styles = StyleSheet.create({
    mainContainer: {
        width:85,
        height:45,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        marginLeft:5,
        marginRight:5
    },
    available:{
        backgroundColor: '#FC5465'
    },
    notAvailable:{
        backgroundColor:'#9C9899'
    },
    timeText:{
        color:'white'
    }
});
export default styles;
