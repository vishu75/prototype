//Author : Rohan Patel
import React from "react";
import {Image, SafeAreaView, Text, View} from "react-native";
import Styles from "./CreditCardStyle";
import {LinearGradient} from "expo-linear-gradient";
import styles from "../../Screens/CartScreen/CartScreenStyle";

const CreditCard = props => {
    return (
        <SafeAreaView style={Styles.mainContainer}>
            <LinearGradient style={Styles.cardBody}
                            colors={["#434343", "#000000"]}
                            start={[0.0, 0.5]}
                            end={[1.0, 0.5]}
                            locations={[0.0, 1.0]}
                            useAngle={true} angle={45} angleCenter={{x: 0.5, y: 0.5}}>

                <View style={Styles.sec1}>
                    <Image source={require("../../assets/visaWhite.png")}
                           style={{height: 35, width: 50, marginRight: 35, marginTop: 30}}/>
                </View>
                <View style={Styles.sec2}>
                    <Text style={Styles.cardNumber}>{props.card.number}</Text>
                </View>
                <View style={Styles.sec3}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end'}}>
                        <Text style={Styles.nameOnCard}>{props.card.name}</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text style={Styles.expText}>EXP</Text>
                        <Text style={Styles.cardExp}>{props.card.exp}</Text>
                    </View>
                </View>

            </LinearGradient>
        </SafeAreaView>
    );
};

export default CreditCard;