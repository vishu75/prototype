//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    cardBody: {
        borderRadius: 25,
        height: 220,
        width: '90%'
    },
    sec1: {
        flex: 2,
        alignItems: 'flex-end'
    },
    sec2: {
        flex: 1
    },
    sec3: {
        flex: 2,
        flexDirection: 'row'
    },
    cardNumber: {
        color: 'white',
        fontWeight: '200',
        fontSize: 23,
        marginLeft: 30
    },
    nameOnCard: {
        color: 'white',
        fontWeight: '200',
        fontSize: 20,
        marginLeft: '18%',
        marginBottom: '15%'
    },
    expText: {
        color: 'white',
        fontWeight: '200',
        fontSize: 13,
        marginRight: '28%',
        marginTop: '7%'
    },
    cardExp: {
        color: 'white',
        fontWeight: '200',
        fontSize: 17,
        marginRight: '18%',
        marginTop: '5%%'
    }
});
export default styles;
