//Author : Jay Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: 'white',
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    innerContainer: {
        backgroundColor: 'white',
        borderRadius: 20,
        justifyContent: 'center',
        flexDirection: 'row',
        height: '100%',
        width: '90%',
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.1,
        shadowRadius: 10,
        elevation: 3
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    numberContainer: {
        flex: 3,
        justifyContent: 'center'
    },
    actionContainer: {
        flex: 1,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        /*backgroundColor:'#FE6460',*/
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: 45,
        width: '70%'
    },
    number: {
        marginLeft: '5%',
        fontSize: 18,
        fontWeight: '200'
    },
    radioButton: {
        height: 15,
        width: 15,
        borderRadius: 25,
        backgroundColor: '#373B95',
        borderWidth: 0.5,
        borderColor: '#373B95'
    },

});
export default styles;
