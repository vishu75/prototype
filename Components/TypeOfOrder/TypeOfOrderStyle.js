//Author : Rohan Patel
import {StyleSheet, Dimensions} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center'
    },
    message: {
        fontSize: 20,
        fontWeight: '600',
        marginTop:'5%'
    },
    buttonTouchable:{
        marginLeft:'10%',
        marginRight:'10%',
        alignItems:'center',
        paddingLeft:'6%',
        paddingRight:'6%',
        paddingTop:'4%',
        paddingBottom:'4%',
        borderRadius:50,
        backgroundColor:'#FC5565'
    },
    buttonText:{
        fontSize: 16,
        color:'white',
        fontWeight: '500'
    }
});

export default styles