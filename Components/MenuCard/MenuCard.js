//Author : Rohan Patel
import React from "react";
import {Image, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import StarRating from "react-native-star-rating";
import styles from "./MenuCardStyle.js";

const MenuCard = props => {
    return (
        <View style={styles.menuCard}
              onResponderRelease={() => props.onClickHandler(props.dish)}
              onStartShouldSetResponder={() => true}>
            {/*Image Section of menucard*/}
            <View style={{flexDirection: "row"}}>
                <View style={styles.menuImageView}>
                    <Image
                        style={styles.menuImage}
                        source={{
                            uri: `${props.dish.image}`
                        }}
                    />
                </View>

                {/*Info section*/}
                <View style={styles.menuText}>
                    <Text style={styles.foodName}>{props.dish.name}</Text>

                    {/*Star and review*/}
                    <View style={{flex: 1, flexDirection: "row", marginTop: 10}}>
                        <View style={{flex: 1}}>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={4.5}
                                fullStarColor={"#FAC915"}
                                starSize={15}
                            />
                        </View>
                        <View style={{flex: 2, paddingLeft: 10}}>
                            <Text style={{color: "grey"}}>
                                {props.dish.star} {"(" + props.dish.reviews + " Reviews)"}
                            </Text>
                        </View>
                    </View>
                    {/*star and review end*/}

                    {/*Like and price*/}
                    <View style={{flex: 1}}>
                        <View style={styles.priceView}>
                            <Text style={styles.price}>${props.dish.price}</Text>
                        </View>
                    </View>
                    {/*    Like and price end*/}
                </View>
                {/*  End of info section*/}
            </View>

            {/*<View style={{flex: 1}}>*/}
            {/*    <TouchableOpacity*/}
            {/*        style={styles.addToCartButton}*/}
            {/*        onPress={() => props.addToCartButtonHandler(props.dish)}*/}
            {/*    >*/}
            {/*        <Text*/}
            {/*            style={{*/}
            {/*                color: "white",*/}
            {/*                fontSize: 17,*/}
            {/*                fontWeight: "400",*/}
            {/*                textAlign: "center"*/}
            {/*            }}*/}
            {/*        >*/}
            {/*            Add to cart*/}
            {/*        </Text>*/}
            {/*        <Image*/}
            {/*            source={require("../../assets/basket.png")}*/}
            {/*            style={{height: 20, width: 20, marginLeft: 9}}*/}
            {/*        />*/}
            {/*    </TouchableOpacity>*/}
            {/*</View>*/}
        </View>
    );
};
export default MenuCard;
