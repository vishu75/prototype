//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    menuCard: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        paddingRight: 10,
        borderRadius: 15,
        backgroundColor: "white",

    },
    menuImageView: {
        flex: 2,
        shadowColor: "#000",
        shadowOffset: {width: 3, height: 3},
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 3
    },
    menuImage: {
        width: "92%",
        height: 100,
        borderRadius: 15
    },
    menuText: {
        flex: 3,
        paddingTop: 10
    },
    foodName: {
        fontWeight: "600",
        fontSize: 18
    },
    priceView: {
        flex: 1,
    },
    price: {
        fontWeight: "900"
    },
    likeView: {
        flex: 1,
        flexDirection: "row"
    },
    likes: {
        fontSize: 15,
        paddingLeft: 10
    },
    addToCartButton: {
        marginTop: 10,
        paddingTop: 6,
        flex: 1,
        flexDirection: "row",
        backgroundColor: "#595FFF",
        height: 35,
        borderRadius: 15,
        justifyContent: "center"
    }
});
export default styles;
