//Author : Rohan Patel
import {StyleSheet, Dimensions} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginBottom: 30,
        backgroundColor: 'white',
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 0.1,
        shadowRadius: 10,
        elevation: 3,
    },
    logoContainer: {
        position: 'absolute',
        top: -65,
        backgroundColor: 'black',
        height: Math.round((dimension.width * 1.8) / 10),
        width: Math.round((dimension.width * 1.8) / 10),

    },
    imageContainer: {
        overflow: 'hidden',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    imageStyle: {
        resizeMode: 'cover',
        width: dimension.width,
        height: Math.round((dimension.width * 6) / 16)
    },
    name: {
        fontSize: 25,
        fontWeight: '700',
        marginTop: 20
    },
    address: {
        marginTop: 6,
        fontSize: 16,
        color: '#676767'
    },
    distance: {
        marginTop: 2,
        fontSize: 16,
        color: '#676767'
    },
    dataView: {
        marginBottom: 30,
        paddingLeft: 15,
        paddingRight: 15
    },
    ratingView: {
        marginTop: 20,
        borderRadius: 5,
        backgroundColor: '#FC5565',
        height: Math.round((dimension.width * 1.1) / 10),
        width: Math.round((dimension.width * 1.1) / 10),
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    }
});

export default styles