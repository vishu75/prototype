//Author : Jay Patel
import React from "react";
import { StyleSheet, View } from "react-native";

import color from "../config/colors";

function ListItemSeperator() {
  return <View style={styles.line}></View>;
}

export default ListItemSeperator;

const styles = StyleSheet.create({
  line: {
    width: "100%",
    height: 0.3,
    backgroundColor: color.gray,
  },
});
