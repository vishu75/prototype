//Author : Jay Patel
import React from "react";
import { View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { SimpleLineIcons } from "@expo/vector-icons";

export default function Icon({
  name,
  sizes,
  backgroundColor = "#000",
  iconColors = "white",
}) {
  return (
    <View
      style={{
        width: sizes,
        height: sizes,
        borderRadius: sizes / 2,
        backgroundColor,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MaterialCommunityIcons
        name={name}
        color={iconColors}
        size={sizes * 0.5}
      />
    </View>
  );
}
