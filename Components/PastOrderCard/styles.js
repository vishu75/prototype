//Author : Jay Patel
import React from "react";
import { StyleSheet, Platform } from "react-native";

const styles = StyleSheet.create({
  text: {
    color: "black",

    fontSize: 18,
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
  },
});

export default styles;
