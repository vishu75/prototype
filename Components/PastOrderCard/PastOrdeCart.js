//Author : Jay Patel
import React from "react";
import { render } from "react-dom";
import {Image, Text,View,TouchableOpacity,TouchableHighlight,
} from "react-native";
import styles from "./PastOrderCardStyle";

import { MaterialCommunityIcons } from '@expo/vector-icons';
import Swipeable from "react-native-gesture-handler/Swipeable";
import moment from "moment-timezone";

export default function PastOrderCard ({title,
                                           subTitle,
    totalItems,
                                           time,
                                           image,
                                           onPress,
                                           renderDeleteItem,
                                           ImageComponent}){

    const convertToLocalTimeZone = time =>{
        const momentTime = moment.utc(time).tz("America/Toronto")
        const month = momentTime.format("MM");
        const date = momentTime.format("DD");
        const convertedTimeZone = moment().month(month).format("MMM")+" "+date
        return convertedTimeZone;
    };
    return(

        <Swipeable renderRightActions={renderDeleteItem}>
            <TouchableHighlight style={{color : '#f8f4f4'}} onPress={onPress}>
                <View style={styles.container}>
                    {ImageComponent}
                    {image && <Image style={styles.image} source={{uri:`${image}`}} />}
                    <View style={styles.DetailsContainer}>
                        <Text style={styles.title} numberOfLines={1}>
                            {title}
                        </Text>
                        {subTitle && (
                            <Text style={styles.subTitle} numberOfLines={2}>
                                {totalItems} Items <Text style={{fontWeight:"900"}}>&#183;</Text> {subTitle}
                            </Text>
                        )}
                        {time && (
                            <Text style={styles.subTitle} numberOfLines={2}>
                                {convertToLocalTimeZone(time)} <Text style={{fontWeight:"900"}}>&#183;</Text> Completed
                            </Text>
                        )}

                    </View>
                    <TouchableOpacity>
                        {/*<MaterialCommunityIcons*/}
                        {/*    style={styles.icon}*/}
                        {/*    name="chevron-right"*/}
                        {/*    size={35}*/}
                        {/*/>*/}
                    </TouchableOpacity>
                </View>
            </TouchableHighlight>
        </Swipeable>
    )
}



