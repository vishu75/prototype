//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    menuCard: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        padding: 10,
        backgroundColor: "white",
        flexDirection: 'row'

    },
    text: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    priceText: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#D0D0D0'
    },
    note: {
        color: 'grey',
        marginTop: 8,
        fontStyle: 'italic'
    },
    dishImage: {
        width: 70,
        height: 70,
        resizeMode: "cover",
        display: "flex",
        flexDirection: "row",
        borderRadius: 25
    }
});
export default styles;
