//Author : Rohan Patel
import React from 'react';
import {Placeholder, PlaceholderLine, PlaceholderMedia, ShineOverlay} from "rn-placeholder";
import {responsiveHeight, responsiveWidth} from "react-native-responsive-dimensions";

const MenuCardSkeleton = props => {
    return (
        <Placeholder
            Animation={ShineOverlay}
            style={{
                marginVertical: 15,
                marginHorizontal: 15,
                borderRadius: 4
            }}
            Left={props => (
                <PlaceholderMedia
                    style={[
                        props.style,
                        {
                            width: responsiveWidth(30),
                            height: responsiveHeight(10)
                        }
                    ]}
                />
            )}
        >
            <PlaceholderLine style={{marginTop: responsiveHeight(1)}} width={70}/>
            <PlaceholderLine style={{marginTop: responsiveHeight(1.5)}} width={50}/>
            <PlaceholderLine width={50}/>
        </Placeholder>
    );
}
export default MenuCardSkeleton;