//Author : Rohan Patel
import React from 'react';
import {SafeAreaView, Text, ImageBackground} from "react-native";
import styles from "./CategoryCardStyle";

const CategoryCard = props => {
    let containerStyle = styles.mainContainer;
    let textStyle = styles.name;

    if (props.selected == true) {
        containerStyle = styles.mainContainerSelected;
        textStyle = styles.nameSelected;
    }

    return (
        <SafeAreaView style={containerStyle}
                      onResponderRelease={() => props.onClickHandler(props.category.id)}
                      onStartShouldSetResponder={() => true}>
            <Text style={textStyle}>{props.category.name}</Text>
        </SafeAreaView>
    );
};

export default CategoryCard;