//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#F0F1F3',
        width: '100%',
        height: '100%',
        borderRadius: 20,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: '8%'
    },
    normalFontBill: {
        fontSize: 15,
        color: '#929395'
    }
});
export default styles;
