//Author : Rohan Patel
import React, {useState} from "react";
import {View,Text,Modal,TouchableOpacity} from "react-native";
import styles from  "./DateTimePickerModalStyle"
import DateTimePicker from "@react-native-community/datetimepicker";
import { Feather } from '@expo/vector-icons';

const DateTimePickerModal = props=>{
    const curDate = new Date();
    const [date, setDate] = useState(new Date(curDate.getFullYear(),curDate.getMonth(),curDate.getDate()-1));
    const [mode, setMode] = useState('datetime');
    const [show, setShow] = useState(true);
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        console.log("CURRENT DAYE IS : "+currentDate);
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const changePress = ()=>{
        props.onChangePressHandler(date);
        props.onCancelPressHandler();
    }
    return(
        <View style={styles.mainContainer}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.mainContainer}>
                    <View style={styles.modalView}>
                        <View style={{alignItems:'center'}}><Text style={{fontSize:18,fontWeight:'600'}}>Change Takeout Time</Text></View>
                        {show && <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={mode}
                            maximumDate={new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate()+1,curDate.getHours())}
                            minimumDate={curDate}
                            is24Hour={true}
                            display="default"
                            onChange={onChange}
                            style={{marginTop:20}}
                        />}

                        <View style={{flexDirection:'row',marginTop:15}}>
                            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                <TouchableOpacity onPress={()=>props.onCancelPressHandler()}>
                                    <Text style={{color:'red',fontWeight:'600'}}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1,alignItems:'center'}}>
                                <TouchableOpacity style={{backgroundColor:'#FC5565',paddingTop:10,paddingBottom:10,paddingRight:15,paddingLeft:15,borderRadius:10}}
                                onPress={()=>changePress()}>
                                    <Text style={{color:'white',fontWeight:'600'}}>Done</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{flexDirection:'row',marginTop:20}}>
                            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                <Feather name="info" size={19} color="black" /></View>
                            <View style={{flex:4,justifyContent:'center'}}>
                                <Text style={{fontSize:11}}>You can book no more than 24hrs in advance</Text>
                            </View>
                        </View>

                    </View>
                </View>
            </Modal>
        </View>
    )
};

export default DateTimePickerModal