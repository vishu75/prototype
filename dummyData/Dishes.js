const Dishes = [
    // Max 17 character for dish name
    {
        id: 1,
        name: "Chicken Tandoori",
        image:
            "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F9%2F2014%2F04%2Foriginal-201309-HD-chicken-dishes-nomad-new-york-city.jpg",
        star: 3.5,
        reviews: "257",
        likes: 412,
        price: 19.24
    },
    {
        id: 2,
        name: "Paneer Pizza",
        image:
            "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F9%2F2014%2F04%2Foriginal-201309-HD-chicken-dishes-nomad-new-york-city.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    },
    {
        id: 3,
        name: "Paneer Tikka",
        image:
            "https://myfancypantry.files.wordpress.com/2013/01/restraunt-style-paneer-tikka-masala.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    },
    {
        id: 4,
        name: "Mix Veg",
        image:
            "https://sweetspotnutrition.ca/wp-content/uploads/2019/01/eat-variety-healthy-foods-image-copy.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    },
    {
        id: 5,
        name: "Seekh Kebab",
        image:
            "https://i.pinimg.com/originals/87/5c/fd/875cfd391f8dfc4d20fc243b884330b6.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    },
    {
        id: 6,
        name: "Dal Tadka",
        image:
            "https://previews.123rf.com/images/timolina/timolina1902/timolina190200229/116755008-indian-dal-traditional-indian-soup-lentils-indian-dhal-spicy-curry-in-bowl-spices-herbs-rustic-black.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    },
    {
        id: 7,
        name: "Lasagnia",
        image:
            "https://www.connoisseurusveg.com/wp-content/uploads/2019/06/vegan-lasagna-6-of-7.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    },
    {
        id: 8,
        name: "Alfredo Pasta",
        image:
            "https://d3hne3c382ip58.cloudfront.net/files/uploads/bookmundi/resized/cmsfeatured/pasta-1509527885-785X440.jpg",
        star: 4.5,
        reviews: "757",
        likes: 12,
        price: 11.77
    }

];
export default Dishes;
