//Author : Rohan Patel
import {UPDATE_RESTAURANT} from "./restaurantActionsTypes";

export const updateRestaurant = (restaurantObj) => {
    return {
        type: UPDATE_RESTAURANT,
        payload: restaurantObj
    }
};