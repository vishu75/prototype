//Author : Rohan Patel
import {ADD_ORDER, UPDATE_EMAIL, UPDATE_FULLNAME, UPDATE_PASSWORD, UPDATE_USER} from "../Actions/userActionsTypes";

const initialState = { fullname:"",email: "", password: "",orders:[]};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case UPDATE_FULLNAME:
      state.fullname = payload;
      return state;
    case UPDATE_EMAIL:
      state.email = payload;
      console.log(state);
      return state;
    case UPDATE_PASSWORD:
      state.password = payload;
        console.log(state);
      return state;
    case UPDATE_USER:return payload;
    case ADD_ORDER:
      state.orders.push(payload);
      return state;
    default:
      return state;
  }
};

export default userReducer;
