//Author : Rohan Patel
import {ADD_ORDER, UPDATE_EMAIL, UPDATE_FULLNAME, UPDATE_PASSWORD, UPDATE_USER} from "./userActionsTypes";

export const updateFullName = (newName)=>{
    return {
      type: UPDATE_FULLNAME,
        payload:newName
    };

};

export const updateEmail = (newEmail) => {
    return {
        type: UPDATE_EMAIL,
        payload: newEmail
    }
};

export const updatePassword = (newPassword) => {
    return {
        type: UPDATE_PASSWORD,
        payload: newPassword
    }
};

export const updateUser = (newUser) => {
  return {
      type : UPDATE_USER,
      payload: newUser
  }
};

export const addOrder = (order)=>{
  return {
      type:ADD_ORDER,
      payload: order
  }  ;
};