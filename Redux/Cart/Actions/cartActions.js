//Author : Rohan Patel
import {ADD_TO_CART, CLEAR_CART, REMOVE_DISH} from "./cartAcionsTypes";

export const addToCart = (dish) => {
    return {
        type: ADD_TO_CART,
        payload: dish
    }
};

export const clearCart = () => {
    return {
        type: CLEAR_CART,
        payload: []
    }
};

export const removeDish = (id) => {
    return {
        type : REMOVE_DISH,
        payload : id
    }
};
