//Author : Rohan Patel
import {ADD_ORDER_TYPE} from "./orderActionsTypes";

export const addOrderType = (orderType) => {
    return {
        type: ADD_ORDER_TYPE,
        payload: {orderType:orderType}
    }
};