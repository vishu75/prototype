//Author : Rohan Patel
import React from "react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Ionicons} from "@expo/vector-icons";
import ScanScreenContainer from "../ScanScreenContainer/ScanScreenContainer";
import AccountScreenContainer from "../AccountScreenContainer/AccountScreenContainer";
import HomeScreenContainer from "../HomeScreenContainer/HomeScreenContainer";

const TabNavigationBar = (props) => {
    const Tab = createBottomTabNavigator();

    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;

                    switch (route.name) {
                        case "Home":
                            iconName = "ios-home";
                            break;
                        case "Scan":
                            iconName = "md-qr-scanner";
                            break;
                        case "Account":
                            iconName = "md-person";
                            break;
                        default:
                            iconName = "";
                            break;
                    }


                    return <Ionicons name={iconName} size={size} color={color}/>;
                },
            })}
            tabBarOptions={{
                activeTintColor: "#FC5565",
                inactiveTintColor: "gray",
            }}
        >
            <Tab.Screen name="Home" component={HomeScreenContainer}/>
            <Tab.Screen name="Scan" component={ScanScreenContainer}/>
            <Tab.Screen name="Account" component={AccountScreenContainer}/>
        </Tab.Navigator>
    );
};

export default TabNavigationBar;
