//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white",
    },
    textContainer: {
        flex: 2,
        paddingTop: "15%",
        paddingLeft: "5%",
    },
    animationContainer: {
        flex: 3,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonContainer: {
        flex: 2,
        alignItems: "center",
        justifyContent: "center",
    },
    textOne: {
        fontSize: 50,
        fontWeight: "bold",
    },
    textTwo: {
        fontSize: 50,
        fontWeight: "bold",
    },
    getStartedContainer: {
        height: 58,
        alignItems: "center",
        justifyContent: "center",
        width: "70%",
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 0.1,
        shadowRadius: 48,
        elevation: 3,
    },
    gradientContainer: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30,
    },
    getStartedTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    getStartedText: {
        color: "white",
        fontSize: 19,
        fontWeight: "bold",
    },
});

export default styles;
