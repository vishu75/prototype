//Author : Rohan Patel
import React from "react";
import {
    Alert,
    Image,
    SafeAreaView,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import styles from "./AccountScreenStyle";
import {updateUser} from "../../Redux/User/Actions/userActions";
import {updateRestaurant} from "../../Redux/Restaurant/Actions/restaurantActions";
import {connect} from "react-redux";
import {AntDesign, MaterialIcons,SimpleLineIcons, MaterialCommunityIcons} from '@expo/vector-icons';


const AccountScreen = (props) => {
    const onLogoutPressHandler = () => {
        Alert.alert("Logout Successfully", "", [
            {
                text: "OK",
                onPress: () => {
                    props.onClearUser({email: "", password: "", orders: []});
                    props.navigation.navigate("Login");
                },
            },
        ]);
    };
    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={{flex: 1, marginTop: 70}}>

                <TouchableOpacity style={styles.logoutButton}
                                  onPress={() => props.navigation.navigate("SavedPaymentMethods")}>
                    <AntDesign name="creditcard" size={25} color="black" />
                    <Text style={{fontSize: 20, fontWeight: '400', marginLeft: 10}}>Saved Payment Methods</Text>
                </TouchableOpacity>


                <TouchableOpacity style={styles.logoutButton}
                                  onPress={() => props.navigation.navigate("PastOrders")}>
                    <SimpleLineIcons name="bag" size={24} color="black" />
                    <Text style={{fontSize: 20, fontWeight: '400', marginLeft: 10}}>Past Orders</Text>
                </TouchableOpacity>


                <TouchableOpacity
                    style={styles.logoutButton}
                    onPress={() => onLogoutPressHandler()}
                >
                    <MaterialCommunityIcons name="logout" size={24} color="red"/>
                    <Text style={{fontSize: 20, color: 'red', fontWeight: 'bold', marginLeft: 10}}>Logout</Text>
                </TouchableOpacity>

            </View>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});
const mapActionsToProps = {
    onClearUser: updateUser,
    onRemoveRestaurant: updateRestaurant,
};
export default connect(mapStateToProps, mapActionsToProps)(AccountScreen);
