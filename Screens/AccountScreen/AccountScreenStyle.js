//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white",
    },
    logoutButton: {
        flexDirection: 'row', alignItems: 'center', marginLeft: '5%', marginBottom: 15
    },
});
export default styles;
