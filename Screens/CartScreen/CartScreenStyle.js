//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "#FC5565",
    },
    view1: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    view2: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50
    },
    normalFontBill: {
        fontSize: 15,
        color: '#929395'
    },
    sectionTitle: {
        fontSize: 38,
        fontWeight: "700",
        margin: 15,
        textAlign: "center",
    },
    placeOrderButton: {
        backgroundColor: "green",
        padding: 10,
        margin: 20,
        borderRadius: 15,
        alignItems: "center",
    },
    textGrey: {
        color: "grey",
        marginTop: 5,
        fontSize: 15,
    },
    totalText: {
        marginTop: 5,
        fontSize: 15,
        fontWeight: "bold",
    },
    buttonContainer: {
        flex: 0.7,
        width: dimension.width,
        height: 100,
        paddingLeft: "10%",
        paddingRight: "10%",
        justifyContent: "center",
        alignItems: 'center',

    },
    checkoutButtonContainer: {
        height: 58,
        alignItems: "center",
        justifyContent: "center",
        width: "80%",
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 0.1,
        shadowRadius: 48,
        elevation: 3
    },
    gradientContainer: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30,
    },
    checkoutTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: 'row'
    },
    checkoutText: {
        color: "white",
        fontSize: 19,
        fontWeight: "bold",
        marginRight: 20
    }
});
export default styles;
