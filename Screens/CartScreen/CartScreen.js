//Author : Rohan Patel
import React, {useState, useEffect} from "react";
import {
    FlatList,
    View,
    SafeAreaView,
    Text,
    TouchableOpacity, Alert,
} from "react-native";
import styles from "./CartScreenStyle";
import axios from "axios";
import baseUrl from "../../dummyData/baseURL";
import OrderItemCard from "../../Components/OrderItemCard/OrderItemCard";
import {connect} from "react-redux";
import {clearCart, removeDish} from "../../Redux/Cart/Actions/cartActions";
import {addOrder} from "../../Redux/User/Actions/userActions";
import {LinearGradient} from "expo-linear-gradient";
import {Ionicons} from "@expo/vector-icons";
import CartTotalCard from "../../Components/CartTotalCard/CartTotalCard";


const CartScreen = (props) => {
    let data = props.dishes;
    let restaurantID = props.restaurant.id;

    const [cartTotal, setCartTotal] = useState(0);
    const [hst, setHst] = useState(0);

    const countCartTotal = () => {
        let h = 0;

        for (let i = 0; i < data.length; i++) {
            let price = data[i].dish.price;
            h += ((price * data[i].dish.tax) / 100) * data[i].quantity;
        }
        h = parseFloat(h.toFixed(2));
        setHst(h);
        setCartTotal(parseFloat((props.subTotal + h).toFixed(2)));
    };

    useEffect(() => {
        countCartTotal();
    }, [props.dishes]);

    const placeOrderHandler = (orderType) => {
        console.log("PLACE ORDER BUTTON CLICKED");
        console.log("passes order type is ")
        console.log(orderType)
        let orderItems = [];
        for (const dish of data) {
            // console.log(dish);
            let newObj = {
                ...dish,
            };
            delete newObj["id"];
            orderItems.push(newObj);
        }

        let holder = {
            items:orderItems
        }
        if(props.orderType === 0){
            holder.dineInOrder = orderType
        }
        if(props.orderType === 1){
            holder.takeoutOrder = orderType
        }
        if(props.orderType === 2){
            holder.inRestaurantOrder = orderType
            console.log("ORDER TYPE IS")
            console.log(orderType)
        }

        console.log(holder);
        axios
            .post(
                baseUrl + "/order/" + restaurantID + "/" + props.user.id,
                holder
            )
            .then((res) => {
                Alert.alert(
                    "Order placed",
                    "We have received your order, hope you will enjoy the meal",
                    [
                        {
                            text: "Ok",
                            style: "cancel"
                        }
                    ],
                    { cancelable: false }
                );
                props.onClearCart();
                props.navigation.navigate("MenuScreen");
            })
            .catch((err) => {
                console.log(err);
            });
    };


    const onCheckoutClickHandler = () => {
        //Proceed only if cart is not empty
        if(data.length!==0) {
            if (props.orderType == 0) {
                props.navigation.navigate("TableReservation", {
                    subTotal: props.subTotal,
                    hst: hst,
                    cartTotal: cartTotal,
                    placeOrderHandler: placeOrderHandler
                })
            }
            if (props.orderType == 1) {
                props.navigation.navigate("TakeoutInfo", {
                    subTotal: props.subTotal,
                    hst: hst,
                    cartTotal: cartTotal,
                    placeOrderHandler: placeOrderHandler
                })
            }
            if (props.orderType == 2) {
                props.navigation.navigate("Checkout", {
                    amount: cartTotal,
                    data: data,
                    placeOrderHandler: placeOrderHandler
                })
            }
        }
    }

    const onRemoveDish = id=>{
        const cartWillBeEmpty = data.length === 1;
        props.onRemoveDish(id);
        if(cartWillBeEmpty)
            props.navigation.navigate("MenuScreen");
    };

    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={styles.view1}>
                <Text style={{color: 'white', fontSize: 35, fontWeight: '500', marginLeft: '10%', marginBottom: '8%'}}>My
                    Order</Text>
            </View>
            <View style={styles.view2}>
                <View style={{flex: 1}}>
                    <FlatList
                        data={data}
                        renderItem={(item) => (
                            <OrderItemCard
                                dish={item.item}
                                removeDish={() => onRemoveDish(item.item.id)}
                                delete={false}
                            />
                        )}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item) => item.id.toString()}
                        style={{marginTop: 50, marginLeft: '5%', marginRight: '5%'}}
                    />
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{flex: 0.7, width: '90%'}}><CartTotalCard subTotal={props.subTotal} hst={hst}
                                                                           cartTotal={cartTotal}/></View>
                    <View style={styles.buttonContainer}>
                        <View style={styles.checkoutButtonContainer}>
                            <LinearGradient
                                colors={["#FE2382", "#FA8748"]}
                                style={styles.gradientContainer}
                                start={[0.0, 0.5]}
                                end={[1.0, 0.5]}
                                locations={[0.0, 1.0]}
                            >
                                <TouchableOpacity
                                    style={styles.checkoutTouchable}
                                    onPress={() => onCheckoutClickHandler()}
                                >
                                    <Text style={styles.checkoutText}>CHECKOUT</Text>
                                    <Ionicons name={'ios-card'} size={30} color={'white'}/>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                    </View>


                </View>
            </View>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => ({
    dishes: state.cart.items,
    subTotal: state.cart.total,
    restaurant: state.restaurant,
    user: state.user,
    orderType: state.order.orderType
});
const mapActionsToProps = {
    onClearCart: clearCart,
    onRemoveDish: removeDish,
    onAddOrderCreated: addOrder,
};
export default connect(mapStateToProps, mapActionsToProps)(CartScreen);
