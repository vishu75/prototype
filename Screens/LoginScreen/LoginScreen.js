//Author : Rohan Patel
import React, {useState} from "react";
import {
    SafeAreaView,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Alert,
} from "react-native";
import {connect} from "react-redux";
import {Input} from "react-native-elements";
import styles from "./LoginScreenStyle";
import {
    updateEmail,
    updatePassword,
    updateUser,
} from "../../Redux/User/Actions/userActions";
import axios from "axios";
import baseURL from "../../dummyData/baseURL";
import {Base64} from "js-base64";
import {OutlinedTextField} from "react-native-material-textfield";
import {LinearGradient} from "expo-linear-gradient";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

const LoginScreen = (props) => {
    const [incorrectLogin, setIncorrectLogin] = useState(false);
    const onLoginPressHandler = () => {
        props.onPasswordUpdate(Base64.encode(props.user.password));
        console.log(props.user);
        axios
            .post(baseURL + "/user/login", props.user)
            .then((res) => {
                if (res.data === "") {
                    setIncorrectLogin(true);
                    // Alert.alert(
                    //     'Incorrect credentials',
                    //     'please try again',
                    //     [
                    //         {text: 'OK'},
                    //     ]
                    // );
                } else {
                    props.onUserUpdate(res.data);
                    console.log(props.user);
                    Alert.alert("Login Successful", "lets feed your hunger", [
                        {
                            text: "OK",
                            onPress: () => props.navigation.navigate("Dashboard"),
                        },
                    ]);
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <KeyboardAwareScrollView
            style={{backgroundColor: "white"}}
            resetScrollToCoords={{x: 0, y: 0}}
            contentContainerStyle={styles.mainContainer}
            scrollEnabled={false}
        >
            <SafeAreaView style={{flex: 2}}>
                <Image
                    source={require("../../assets/croods-happy.png")}
                    style={styles.registerImage}
                    resizeMode={"contain"}
                />
            </SafeAreaView>

            <View style={styles.loginSection}>
                <Text
                    style={{
                        fontSize: 40,
                        fontWeight: "bold",
                        marginTop: 25,
                        marginLeft: 25,
                    }}
                >
                    Login!
                </Text>
                <View style={{flex: 1, alignItems: "center", marginTop: "15%"}}>
                    {incorrectLogin && (
                        <Text style={{color: "red", fontWeight: "600"}}>
                            Either Email or Password is incorrect
                        </Text>
                    )}
                    <OutlinedTextField
                        label="Email"
                        containerStyle={{width: 320, marginTop: 15}}
                        onChangeText={(text) => props.onEmailUpdate(text)}
                    />
                    <OutlinedTextField
                        label="Password"
                        containerStyle={{width: 320, marginTop: 15}}
                        onChangeText={(text) => props.onPasswordUpdate(text)}
                        secureTextEntry={true}
                    />
                </View>
            </View>
            <SafeAreaView
                style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                }}
            >
                <View style={styles.getStartedContainer}>
                    <LinearGradient
                        colors={["#FE2382", "#FA8748"]}
                        style={styles.gradientContainer}
                        start={[0.0, 0.5]}
                        end={[1.0, 0.5]}
                        locations={[0.0, 1.0]}
                    >
                        <TouchableOpacity
                            style={styles.getStartedTouchable}
                            onPress={() => onLoginPressHandler()}
                        >
                            <Text style={styles.getStartedText}>LOGIN</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            </SafeAreaView>
        </KeyboardAwareScrollView>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

const mapActionsToProps = {
    onEmailUpdate: updateEmail,
    onPasswordUpdate: updatePassword,
    onUserUpdate: updateUser,
};
export default connect(mapStateToProps, mapActionsToProps)(LoginScreen);
