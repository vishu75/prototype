//Author : Rohan Patel
import React, {useState} from "react";
import {FlatList, Image, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import Styles from "./SavedPaymentScreenStyle";
import PaymentMethodCard from "../../Components/PaymentMethodCard/PaymentMethodCard";

const SavedPaymentScreen = props => {
    console.log(props.route.params);
    const [paymentMethods, setPaymentMethods] = useState([
        {id: 1, number: '**** **** **** 1234'},
        {id: 2, number: '**** **** **** 1235'},
        {id: 3, number: '**** **** **** 1236'},
        {id: 4, number: '**** **** **** 1237'}
    ]);

    const onDeletePressHandler = id => {
        let paymentCpy = [...paymentMethods];
        for (let i = 0; i < paymentCpy.length; i++) {
            if (paymentCpy[i].id == id) {
                paymentCpy.splice(i, 1);
                break;
            }
            console.log(paymentCpy);
        }
        setPaymentMethods(paymentCpy);
    };
    return (
        <SafeAreaView style={Styles.mainContainer}>
            {paymentMethods.length != 0 && <Text style={Styles.title}>Saved Payment Methods</Text>}
            {paymentMethods.length != 0 && <FlatList
                data={paymentMethods}
                renderItem={({item}) => <PaymentMethodCard action={"delete"} selected={true} number={item.number}
                                                           onSwipe={() => onDeletePressHandler(item.id)}/>}
                keyExtractor={item => item.id.toString()}
                style={{flex: 3}}
            />}

            {paymentMethods.length != 0 &&
            <View style={Styles.addContainer}
                  onResponderRelease={() => {
                      props.navigation.navigate("AddNewPaymentMethod")
                  }}
                  onStartShouldSetResponder={() => true}
            >
                <Text style={Styles.textAddPayment}>+ Add a new payment method</Text>
            </View>
            }

            {paymentMethods.length === 0 &&
            <View style={Styles.noCardMessageContainer}>
                <Image source={require("../../assets/noPaymentMethod.png")}
                       resizeMode={"cover"}
                       style={Styles.noCardImage}/>
                <Text style={Styles.noCardMessage}>We cannot find any saved payment method on your account.</Text>
                <TouchableOpacity style={Styles.addButton}
                                  onPress={() => props.navigation.navigate("AddNewPaymentMethod")}>
                    <Text style={{color: 'white', fontSize: 17, fontWeight: '500'}}>Add new payment method</Text>
                </TouchableOpacity>
            </View>
            }

        </SafeAreaView>
    );
};

export default SavedPaymentScreen;