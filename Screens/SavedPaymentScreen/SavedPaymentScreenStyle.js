//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 20,
        marginLeft: '6%',
        marginTop: '20%',
        marginBottom: '10%'
    },
    addContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    textAddPayment: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#373B95',
        marginBottom: '5%'
    },
    noCardMessageContainer: {
        flex: 0.8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    noCardImage: {
        marginTop: '10%',
        height: 200,
        width: '70%'
    },
    noCardMessage: {
        width: '70%',
        textAlign: 'center',
        fontSize: 17,
        fontWeight: '500',
        marginTop: '5%'
    },
    addButton: {
        marginTop: 17,
        backgroundColor: '#FF725E',
        borderRadius: 30,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 18,
        paddingBottom: 18
    }
});
export default styles;
