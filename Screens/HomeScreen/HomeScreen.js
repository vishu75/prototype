//Author : Rohan Patel
import React, {useEffect, useState, useRef} from "react";
import {FlatList, Image, SafeAreaView, Text, View, Animated, TouchableOpacity, Alert} from "react-native";
import {SearchBar} from 'react-native-elements';
import styles from "./HomeScreenStyle";
import {connect} from "react-redux";
import {updateUser} from "../../Redux/User/Actions/userActions";
import {updateRestaurant} from "../../Redux/Restaurant/Actions/restaurantActions";
import {useIsFocused} from "@react-navigation/native";
import axios from "axios";
import baseURL from "../../dummyData/baseURL";
import CategoryCard from "../../Components/CategoryCard/CategoryCard";
import * as Haptics from "expo-haptics";
import SearchRestaurantCard from "../../Components/SearchRestaurantCard/SeachRestaurantCard";
import RBSheet from "react-native-raw-bottom-sheet";
import TypeOfOrder from "../../Components/TypeOfOrder/TypeOfOrder";
import {addOrderType} from "../../Redux/Order/Actions/orderActions";
import {clearCart} from "../../Redux/Cart/Actions/cartActions";


const HomeScreen = (props) => {
    const refRBSheet = useRef();

    const [orderCount, setOrderCount] = useState(0);
    //This makes the component to re-render
    // const [focused, setFocused] = useState(useIsFocused());
    const [searchText, setSearchText] = useState("");
    const [selectedRestaurant, setSelectedRestaurant] = useState({});
    const [restaurantType, setRestaurantType] = useState(
        [{id: 1, name: 'Dine In', selected: true}, {id: 2, name: 'Take Out', selected: false}]);

    const [restaurants, setRestaurants] = useState(
        dummyRestaurants
    )

    const onRestaurantTypeChange = id => {
        Haptics.selectionAsync().then(res => console.log(res)).catch(err => console.error(err));
        const c = [...restaurantType];
        for (let i = 0; i < c.length; i++) {
            if (c[i].id == id) {
                c[i].selected = true;
            } else {
                c[i].selected = false;
            }
        }
        setRestaurantType(c);
    };
    const onSearchBarTextChange = text => {
        setSearchText(text);
        // const res = restaurants.filter(obj=>obj.name.includes(text));
        // setRestaurants(res);
    };

    useEffect(() => {
        const runWhenInFocus = props.navigation.addListener('focus', () => {
            props.onAddOrderType(-1);
            props.onClearCart()
            axios
                .get(baseURL + "/restaurant/displayAll")
                .then((res) => {
                    setRestaurants(res.data);
                    console.log(res.data);
                })
                .catch((err) => console.error(err));
        });
        return runWhenInFocus;
    }, []);


    const onRestaurantClickHandler = restaurant => {
        setSelectedRestaurant(restaurant);
        restaurant = {...restaurant, dinein: true, takeout: true}
        if (restaurant.dinein && restaurant.takeout)
            refRBSheet.current.open();
        else if (restaurant.dinein && !restaurant.takeout)
            navigateToMenu(0)
        else
            navigateToMenu(1)

    };

    const onOrderTypeClickHandler = type => {
        navigateToMenu(type);
    }

    const navigateToMenu = orderType => {
        console.log("ORDER TYPE IS : " + orderType);
        props.onAddOrderType(orderType);
        props.onUpdateRestaurant(selectedRestaurant);
        refRBSheet.current.close();
        props.navigation.navigate("MenuScreen", {data: selectedRestaurant});
    }
    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={{flex: 1, justifyContent: "center"}}>
                <Text style={styles.name}>517 Navigator Dr</Text>
            </View>
            <View style={{flex: 7}}>
                <SearchBar
                    placeholder="Search a restaurant"
                    onChangeText={(text) => onSearchBarTextChange(text)}
                    value={searchText}
                    containerStyle={styles.searchBarContainer}
                    inputContainerStyle={styles.searchBar}
                    searchIcon={{color: '#525872'}}
                    cancelIcon={{color: '#525872'}}
                    inputStyle={{color: '#525872'}}
                />
                <View style={{flex: 1}}>
                    <FlatList
                        data={restaurantType}
                        renderItem={item =>
                            <CategoryCard key={item.item.id}
                                          category={item.item}
                                          selected={item.item.selected}
                                          onClickHandler={onRestaurantTypeChange}
                            />
                        }
                        keyExtractor={item => item.id.toString()}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true} style={{marginLeft: 5}}/>
                </View>
                <View style={{flex: 4}}>
                    <FlatList
                        data={restaurants}
                        renderItem={item =>
                            <SearchRestaurantCard restaurant={item.item}
                                                  openOrderTypeDialog={() => onRestaurantClickHandler(item.item)}/>
                        }
                        keyExtractor={item => item.id.toString()}
                        style={{paddingLeft: 10, paddingRight: 10}}
                    />
                </View>
            </View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                customStyles={{
                    wrapper: {
                        backgroundColor: "transparent"
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }
                }}
            >
                <TypeOfOrder onOrderTypePress={(type) => onOrderTypeClickHandler(type)}/>
            </RBSheet>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
    orderType: state.order.orderType
});
const mapActionsToProps = {
    onClearUser: updateUser,
    onRemoveRestaurant: updateRestaurant,
    onAddOrderType: addOrderType,
    onUpdateRestaurant: updateRestaurant,
    onClearCart: clearCart
};
export default connect(mapStateToProps, mapActionsToProps)(HomeScreen);

const dummyRestaurants = [
    {
        id: 1,
        name: 'Honest',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 2,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 3,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 4,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 5,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 6,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 7,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 8,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 9,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 10,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    },
    {
        id: 11,
        name: 'Kwality Grand Indian Buffet',
        logo: 'https://bainbridgega.com/wp-content/uploads/2020/02/djs_popeyes_01.jpg',
        background: 'https://simply-delicious-food.com/wp-content/uploads/2019/04/greek-chicken-salad-4.jpg'
    }
]