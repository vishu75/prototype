//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get('window');

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white"
    },
    mapView: {
        flex: 1
    },
    contentView: {
        flex: 2,
        paddingLeft: 20,
        paddingRight: 20
    },
    yourTakeout: {
        fontWeight: '700',
        fontSize: 20,
        marginTop: 20
    },
    addressContainer: {
        flexDirection: 'row',
        marginTop: 15
    },
    addressTextContainer: {
        flex: 9,
        justifyContent: 'center'
    },
    addressText: {
        fontSize: 15
    },
    directionContainer: {
        flexDirection: 'row',
        marginTop: 15
    },
    directionText: {
        fontSize: 15,
        fontWeight: '600',
        color: 'blue'
    },
    timeContainer: {
        flexDirection: 'row',
        marginTop: 18
    },
    timeTextContainer: {
        flex: 5,
        justifyContent: 'center'
    },
    timeText: {
        fontSize: 15
    },
    timeChangeContainer: {
        flex: 4,
        justifyContent: 'center'
    },
    timeChangeText: {
        fontSize: 15,
        fontWeight: '600',
        color: 'blue'
    }
});
export default styles;
