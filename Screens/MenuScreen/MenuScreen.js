//Author : Rohan Patel
import React, {useState, useEffect} from "react";
import {Text, View, SafeAreaView, FlatList, TouchableOpacity, Image} from "react-native";
import styles from "./MenuScreenStyle";
import CategoryCard from "../../Components/CategoryCard/CategoryCard";
import MenuCard from "../../Components/MenuCard/MenuCard";
import * as Haptics from 'expo-haptics';
import MenuCardSkeleton from "../../Components/MenuCardSkeleton/MenuCardSkeleton";
import {Ionicons} from '@expo/vector-icons';
import {Badge} from "react-native-elements";
import {connect} from "react-redux";
import StarRating from "react-native-star-rating";
import {LinearGradient} from "expo-linear-gradient";

const MenuScreen = props => {
    const data = props.route.params.data;

    const [scroll, setScroll] = useState(false);
    const [categories, setCategories] = useState([]);
    const [dishes, setDishes] = useState([]);
    const [loadingMenu, setLoadingMenu] = useState(true);

    useEffect(() => {

        const c = [...data.menu.categories];
        console.log(c);
        for (let i = 0; i < c.length; i++) {
            if (i == 0) {
                setDishes(c[i].dishes);
                c[i] = {...c[i], selected: true};
            } else {
                c[i] = {...c[i], selected: false};
            }
            console.log("number : " + i);
        }
        setCategories(c);
        setTimeout(() => {
            setLoadingMenu(false);
        }, 500);


    }, []);
    const onCategoryClickHandler = cid => {
        setLoadingMenu(true);
        Haptics.selectionAsync().then(res => console.log(res)).catch(err => console.error(err));
        const c = [...categories];
        for (let i = 0; i < c.length; i++) {
            if (c[i].id == cid) {
                c[i].selected = true;
                setDishes(c[i].dishes);
            } else {
                c[i].selected = false;
            }
        }
        setCategories(c);
        setTimeout(() => {
            setLoadingMenu(false);
        }, 500);
    };

    const onScrollHandler = val => {
        if (val >= 150) {
            setScroll(true);
        }
        if (val <= 10) {
            setScroll(false);

        }
    }

    const onMenuItemClickHandler = d => {
        props.navigation.navigate("MenuItem", {dish: d});
    };

    const onViewOrderClickHandler = () => {
        props.navigation.navigate("CartScreen");
    };

    return (
        <SafeAreaView style={styles.mainContainer}>
            {!scroll &&
            <View style={styles.nameContainer}>
                <Text style={styles.name}>{data.name}</Text>
                <Text style={{color: 'grey', marginTop: 2}}>Fast food, Indian street food</Text>
                <View style={{width: '30%', height: 20, marginTop: 10}}>
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={4.5}
                        fullStarColor={"#FC5565"}
                        starSize={20}
                    />
                </View>
                {props.orderType === 2 &&
                <Text style={{color: '#FC5565', marginTop: 2}}>You are at table : {props.restaurant.table}</Text>}

            </View>
            }
            <SafeAreaView style={styles.contentContainer}>
                <View style={{flex: 1}}>
                    {/*Category list view*/}
                    <View style={{flex: (scroll) ? 0.8 : 1}}>
                        <FlatList
                            data={categories}
                            renderItem={item =>
                                <CategoryCard key={item.item.id}
                                              category={item.item}
                                              selected={item.item.selected}
                                              onClickHandler={onCategoryClickHandler}
                                />
                            }
                            keyExtractor={item => item.id.toString()}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true} style={{marginLeft: 5}}/>
                    </View>
                    {/*Menu items view*/}
                    <View style={{flex: (scroll) ? 6.5 : 5}}>
                        {loadingMenu && <View>
                            <MenuCardSkeleton/>
                            <MenuCardSkeleton/>
                            <MenuCardSkeleton/>
                            <MenuCardSkeleton/>
                        </View>
                        }

                        {!loadingMenu && <FlatList
                            keyExtractor={item => item.id.toString()}
                            data={dishes}
                            renderItem={item => <MenuCard dish={item.item} onClickHandler={onMenuItemClickHandler}/>}
                            onScroll={(e) => onScrollHandler(e.nativeEvent.contentOffset.y)}
                        />}

                        {!loadingMenu && dishes.length == 0 &&
                        <View style={{alignItems: 'center'}}>
                            <Image source={require("../../assets/noItem.png")} style={{height: '70%', width: '70%'}}
                                   resizeMode={"cover"}/>
                            <Text style={{
                                width: '70%',
                                textAlign: 'center',
                                fontSize: 17,
                                fontWeight: '500',
                                marginTop: '5%'
                            }}>Delicious dishes coming soon, please checkout another category</Text>
                        </View>
                        }
                    </View>

                    {/*Order button view*/}
                    {props.cart.items.length != 0 && <View style={{flex: 1.4}}>
                        <View style={styles.buttonContainer}>
                            <LinearGradient
                                colors={["#FE2382", "#FA8748"]}
                                style={styles.gradientContainer}
                                start={[0.0, 0.5]}
                                end={[1.0, 0.5]}
                                locations={[0.0, 1.0]}
                            >
                                <TouchableOpacity style={styles.button} onPress={() => onViewOrderClickHandler()}>
                                    <View style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'row'
                                    }}>
                                        <Ionicons name={"md-basket"} size={24} color={'white'}/>
                                        <Badge
                                            status={"success"}
                                            value={props.cart.items.length}
                                            badgeStyle={{backgroundColor: 'white'}}
                                            textStyle={{color: '#FE2382', fontWeight: 'bold'}}
                                        />
                                    </View>
                                    <View style={{flex: 2, alignItems: 'center'}}>
                                        <Text style={styles.buttonText}>VIEW ORDER</Text>
                                    </View>
                                    <View style={{flex: 1, alignItems: 'center'}}>
                                        <Text style={styles.buttonText}>${props.cart.total.toFixed(2)}</Text>
                                    </View>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                    </View>
                    }
                </View>
            </SafeAreaView>
        </SafeAreaView>
    );
};

const mapStateToProps = state => {
    return {
        cart: state.cart,
        restaurant: state.restaurant,
        orderType: state.orderType
    };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(MenuScreen);
