//Author : Vishvakumar Mavani
import React, {useEffect, useState} from "react";
import {View} from "react-native";
import QRCodeScanner from "../../Components/QRCodeScanner/QRCodeScanner.js";
import styles from "./ScanScreenStyle.js";
import axios from "axios";
import baseURL from "../../dummyData/baseURL";
import {connect} from "react-redux";
import {updateRestaurant} from "../../Redux/Restaurant/Actions/restaurantActions";
import {addOrderType} from "../../Redux/Order/Actions/orderActions";
import {clearCart} from "../../Redux/Cart/Actions/cartActions";

const ScanScreen = (props) => {

    useEffect(() => {
        const runWhenInFocus = props.navigation.addListener('focus', () => {
            props.onAddOrderType(-1);
            props.onClearCart();

        });
        return runWhenInFocus
    }, []);
    const handleBarCodeScan = (type, data) => {
        console.log("Handle Barcode Scan Method called");
        console.log(data);
        try {
            const parsedData = JSON.parse(data);
            const restroID = parsedData.id;
            let table = 0;
            if (restroID === undefined) {
                console.log("Undefined Restaurant ID");
                props.navigation.navigate("IncorrectScan");
                return;
            }
            console.log("Correct QR Code scanned");

            axios
                .get(baseURL + "/restaurant/" + restroID + "/table/" + parsedData.table)
                .then((resOne) => {
                    console.log("FIRST SCAN DATA");
                    console.log(resOne.data);
                    if (resOne.data.code == 0) {
                        table = resOne.data.object.number;
                        axios
                            .get(baseURL + "/restaurant?id=" + restroID)
                            .then((res) => {
                                console.log("Success");
                                const jsonData = res.data;

                                jsonData["table"] = resOne.data["object"];
                                console.log(jsonData);
                                props.onRestaurantUpdate(jsonData);
                                //props.navigation.navigate("SuccessScan");
                                props.onAddOrderType(2);
                                props.navigation.navigate("MenuScreen", {data: jsonData});
                            })
                            .catch((err) => {
                                console.log(err);
                                props.navigation.navigate("IncorrectScan");
                            });
                    } else {
                        console.log("NO MATCH FOUND");
                        console.log(res.data);
                    }
                })
                .catch((err) => console.log(err));
        } catch (e) {
            console.log("QR code is not having JSON data");
            props.navigation.navigate("IncorrectScan");
        }
    };

    return (
        <View style={styles.viewContainer}>
            {
                <QRCodeScanner
                    handleBarCodeScan={(type, data) => handleBarCodeScan(type, data)}
                    orderType={props.orderType}
                />
            }
        </View>
    );
};

const mapStateToProps = (state) => ({
    restaurant: state.restaurant,
    orderType: state.order.orderType
});

const mapActionsToProps = {
    onRestaurantUpdate: updateRestaurant,
    onAddOrderType: addOrderType,
    onClearCart: clearCart
};
export default connect(mapStateToProps, mapActionsToProps)(ScanScreen);
