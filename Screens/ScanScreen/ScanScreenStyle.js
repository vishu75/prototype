//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
    },
    text: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: "300",
        fontSize: 28,
        textAlign: "center",
    },
    qrCodeScanner: {
        flex: 2,
        marginTop: 15,
    },
    qrCodeScannerStart: {
        flex: 1,
        marginTop: 15,
    },
    scanAgainButton: {
        backgroundColor: "blue",
        marginLeft: 70,
        marginRight: 70,
        marginTop: 10,
        borderRadius: 20,
    },
    scanAgainText: {
        color: "white",
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: "center",
    },
    successScan: {
        flex: 3,
        marginLeft: 20,
        marginRight: 20,
        justifyContent: "flex-start",
        alignItems: "center",
    },
    nameText: {
        fontSize: 37,
        textAlign: "center",
    },
    addressText: {
        fontSize: 15,
        textAlign: "center",
        fontStyle: "italic",
    },
    table: {
        fontSize: 20,
        textAlign: "center",
        marginTop: 50,
    },
    hungryButton: {
        backgroundColor: "green",
        paddingLeft: 20,
        paddingRight: 20,
        marginLeft: 70,
        marginRight: 70,
        marginTop: 40,
        borderRadius: 20,
    },
    hungryText: {
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: "center",
        color: "white",
    },
});

export default styles;
