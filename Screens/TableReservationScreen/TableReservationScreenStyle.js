//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get('window');

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white"
    },
    reserveYourSpotText: {
        fontWeight: '700',
        fontSize: 23,
        marginLeft: 15
    },
    list: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: dimension.width,
        marginTop: 30
    },
    inputStyle: {
        width: dimension.width / 2,
        height: 45,
        color: 'black',
        fontSize: 15,
        marginTop: 50,
        marginLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#9C9899'
    },
    contentView: {
        flex: 1.2,
        paddingLeft: 20,
        paddingRight: 20
    },
    yourTakeout: {
        fontWeight: '700',
        fontSize: 20,
        marginTop: 20
    },
    addressContainer: {
        flexDirection: 'row',
        marginTop: 15
    },
    addressTextContainer: {
        flex: 9,
        justifyContent: 'center'
    },
    addressText: {
        fontSize: 15
    },
    directionContainer: {
        flexDirection: 'row',
        marginTop: 15
    },
    directionText: {
        fontSize: 15,
        fontWeight: '600',
        color: 'blue'
    },
    timeContainer: {
        flexDirection: 'row',
        marginTop: 18
    },
    timeTextContainer: {
        flex: 5,
        justifyContent: 'center'
    },
    timeText: {
        fontSize: 15
    },
    timeChangeContainer: {
        flex: 4,
        justifyContent: 'center'
    },
    timeChangeText: {
        fontSize: 15,
        fontWeight: '600',
        color: 'blue'
    }
});
export default styles;
