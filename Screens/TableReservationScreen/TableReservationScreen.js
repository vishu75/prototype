//Author : Rohan Patel
import React, {useEffect, useState} from "react";
import {
    SafeAreaView,
    View,
    Text,
    FlatList,
    TextInput,
    Keyboard,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Alert
} from "react-native";
import {connect} from "react-redux";
import styles from "./TableReservationScreenStyle";
import TimeSlot from "../../Components/TimeSlot/TimeSlot";
import {AntDesign, Entypo, FontAwesome, FontAwesome5} from "@expo/vector-icons";
import CartTotalCard from "../../Components/CartTotalCard/CartTotalCard";
import GradientButton from "../../Components/GradientButton/GradientButton";
import baseURL from "../../dummyData/baseURL";
import axios from "axios";

const TableReservationScreen = props => {

    const [partySize,setPartySize] = useState(2);
    const [timeSlotsForToday,setTimeSlotsForToday] = useState();
    const [requestsMade,setRequestsMade] = useState(false);
    const [selectedTimeSlot,setSelectedTimeSlot] = useState(null);
    const {subTotal} = props.route.params;
    const {hst} = props.route.params;
    const {cartTotal} = props.route.params;
    const {placeOrderHandler} = props.route.params

    console.log(subTotal+" "+hst+" "+cartTotal);

    useEffect(_=>{
        console.log("SIZE IS "+partySize);
        console.log("FETCH STARTED")
        setRequestsMade(false);

        const d = new Date()
        let dayOfWeek = d.getDay()-1;
        if(d.getDay()==0)
            dayOfWeek = 6

        console.log("DAY OF WEEK IS : "+dayOfWeek)
        let timeSlots = [...props.bookingTimeSlots[dayOfWeek].timeSlots];
        const promises = [];
        let today = new Date()
        let todayHours = today.getHours()
        let todayMinutes = today.getMinutes()
        let todaySeconds = today.getSeconds()

        if(todayHours.toString().length == 1)
            todayHours = "0"+todayHours
        if(todayMinutes.toString().length == 1)
            todayMinutes = "0"+todayMinutes
        if(todaySeconds.toString().length == 1)
            todaySeconds = "0"+todaySeconds

        const timeString = todayHours+":"+todayMinutes+":"+todaySeconds
        today = Date.parse("10/10/10 "+timeString)
        for(let index in timeSlots){
            const URL = baseURL+"/restaurant/"+props.restaurantId+"/table/check/"+partySize
            promises.push(axios.post(URL,timeSlots[index]).then(res=>{
                console.log(res.data);
                const timeSlotTime = Date.parse("10/10/10 "+timeSlots[index].time)
                if(today < timeSlotTime && res.data.code == 0)
                    timeSlots[index].available = true;
                else
                    timeSlots[index].available = false;

                timeSlots[index].selected = false;
            }).catch(err=>console.error(err)));
        }
        axios.all(promises).then(data=>{
            console.log("FETCH FINISHED");
            setRequestsMade(true);
            setTimeSlotsForToday(timeSlots);
        })

    },[partySize]);


    const tConvert = time => {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    const onPartySizeChangeHandler = size=>{
        if(Number(size)<2){
            setPartySize(2);
            return;
        }
      setPartySize(Number(size));
    };
    const onTimeSlotClickHandler = i=>{
        if(timeSlotsForToday[i].available) {
            const selectedTimeSlots = [...timeSlotsForToday];
            for (let j in selectedTimeSlots) {
                if (j == i) {
                    selectedTimeSlots[j].selected = true;
                } else {
                    selectedTimeSlots[j].selected = false;
                }
            }
            setTimeSlotsForToday(selectedTimeSlots);
            setSelectedTimeSlot(timeSlotsForToday[i]);
        }
    };


    const onPayClickHandler = _=>{
        //Proceed only if there is a time slot selected
        if(selectedTimeSlot!=null) {
            const URL = baseURL + "/restaurant/" + props.restaurantId + "/table/" + props.userId + "/book/" + partySize + "/1";
            const timeSlot = {...selectedTimeSlot};
            delete timeSlot["available"];
            delete timeSlot["selected"];
            axios.put(URL, timeSlot).then(res => {
                console.log("RESERVATION MADE")
                console.log(res.data);
                placeOrderHandler(res.data["object"]);
            }).catch(err => console.error(err))
        }else{
            Alert.alert(
                "Time slot not selected",
                "Oops, sorry but we cannot book a table unless you select a time slot",
                [
                    {
                        text: "Ok",
                        style: "cancel"
                    }
                ],
                { cancelable: false }
            );
        }
    };
    return (

        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <SafeAreaView style={styles.mainContainer}>
                <View style={{flex: 1}}>
                    <TextInput
                        placeholder={"No. Of people"}
                        keyboardType={"numeric"}
                        style={styles.inputStyle}
                        value={partySize}
                        onChangeText={text=>{onPartySizeChangeHandler(text)}}
                    />

                    {requestsMade && <FlatList
                        data={timeSlotsForToday}
                        renderItem={item => <TimeSlot timeSlot={item}
                        onTimeSlotClickHandler={onTimeSlotClickHandler}/>}
                        onEndReachedThreshold={0}
                        contentContainerStyle={styles.list}
                        keyExtractor={item => item.id.toString()}/> }

                </View>

                <View style={styles.contentView}>
                    <View style={styles.addressContainer}>
                        <View style={{flex: 1}}>
                            <Entypo name="location-pin" size={30} color="black"/>
                        </View>
                        <View style={styles.addressTextContainer}>
                            <Text style={styles.addressText}>517 Navigator Dr, Mississauga, ON, L5W1P5</Text>
                        </View>
                    </View>
                    <View style={styles.directionContainer}>
                        <View style={{flex: 1}}>
                            <FontAwesome5 name="directions" size={24} color="black" style={{paddingLeft: 3}}/>
                        </View>
                        <View style={styles.addressTextContainer}>
                            <TouchableOpacity><Text style={styles.directionText}>GET
                                DIRECTIONS</Text></TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.timeContainer}>
                        <View style={{flex: 1}}>
                            <AntDesign name="clockcircle" size={24} color="black" style={{paddingLeft: 3}}/>
                        </View>
                        <View style={styles.timeTextContainer}>
                            {selectedTimeSlot==null && <Text>---</Text>}
                            {selectedTimeSlot && <Text>Today , {tConvert(selectedTimeSlot.time)}</Text>}
                        </View>
                    </View>
                    <View style={styles.timeContainer}>
                        <View style={{flex: 1}}>
                            <FontAwesome name="credit-card-alt" size={22} color="black" style={{paddingLeft: 2}}/>
                        </View>
                        <View style={styles.timeTextContainer}>
                            <Text style={styles.timeText}>**** **** **** 1234</Text>
                        </View>
                        <View style={styles.timeChangeContainer}>
                            <Text style={styles.timeChangeText}>CHANGE</Text>
                        </View>
                    </View>


                    <View style={{flex: 3, alignItems: 'center', paddingTop: 20}}>
                        <CartTotalCard subTotal={subTotal} hst={hst} cartTotal={cartTotal}/>
                    </View>
                    <SafeAreaView style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
                        <GradientButton text={"Pay Now"} onPressHandler={() => onPayClickHandler()}/>
                    </SafeAreaView>

                </View>
            </SafeAreaView>
        </TouchableWithoutFeedback>

    )
};


const mapStateToProps = (state) => ({
    restaurantId:state.restaurant.id,
    userId:state.user.id,
    bookingTimeSlots: state.restaurant.bookingTimeSlots,
});
const mapActionsToProps = {
};

export default connect(mapStateToProps,mapActionsToProps)(TableReservationScreen);
