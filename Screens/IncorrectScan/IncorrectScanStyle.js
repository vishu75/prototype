//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "#D83D3D",
        alignItems: "center",
    },
    lottieImage: {
        height: "85%",
        padding: "5%",
    },
    scanAgainButton: {
        borderWidth: 2,
        borderColor: "white",
        borderRadius: 25,
        paddingTop: "2%",
        paddingBottom: "2%",
        paddingLeft: "5%",
        paddingRight: "5%",
        marginTop: "5%",
    },
});
export default styles;
