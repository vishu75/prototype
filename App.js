//Author : Rohan Patel
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import LoginScreen from "./Screens/LoginScreen/LoginScreen.js";
import RegisterScreen from "./Screens/RegisterScreen/RegisterScreen.js";
import TabNavigationBar from "./ScreenContainers/TabNavigationBar/TabNavigationBar";

import restaurantReducer from "./Redux/Restaurant/Reducers/restaurantReducer";
import cartReducer from "./Redux/Cart/Reducers/cartReducer";
import userReducer from "./Redux/User/Reducers/userReducer";
import OnboardingScreen from "./Screens/OnboardingScreen/OnboardingScreen";
import AddPaymentMethod from "./Screens/AddPaymentMethodScreen/AddPaymentMethodScreen";
import orderReducer from "./Redux/Order/Reducers/orderReducer";
import TakeoutInfoScreen from "./Screens/TakeoutInfoScreen/TakeoutInfoScreen";
import TableReservationScreen from "./Screens/TableReservationScreen/TableReservationScreen";
import PastOrderCard from "./Components/PastOrderCard/PastOrdeCart";
import PastOrderScreen from "./Screens/PastOrdersScreen/PastOrdeScreen";
// import axios from "axios";
// import jwt from "jsonwebtoken";
//
// const secretKey = "792F423F4528482B4D6251655468576D5A7134743777217A24432646294A404E";
// const issuerId = "462948404D635166546A576E5A7234753778214125442A472D4B614E645267556B58703273357638792F423F4528482B4D6251655368566D597133743677397A";
// const token = jwt.sign({data:'WayOrder',iss:issuerId},secretKey);
// axios.interceptors.request.use(config=> {
//     // Do something before request is sent
//     console.log(token);
//
//     config.headers.Authorization =  token;
//     return config;
// });

const allReducers = combineReducers({
   restaurant : restaurantReducer,
   cart : cartReducer,
  user:userReducer,
    order:orderReducer
});
const store = createStore(allReducers);
const Stack = createStackNavigator();

const App = () => {
  return (
      <Provider store={store}>

    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Onboarding"
        screenOptions={{
          headerShown: false
        }}
      >
          <Stack.Screen name="Takeout" component={TakeoutInfoScreen} options={{headerShown: true,headerBackTitle:'Order',
              headerTitle: '', headerTintColor:'black',headerTransparent:true
          }}/>
          <Stack.Screen name={"payment"} component={AddPaymentMethod}/>
        <Stack.Screen name="Onboarding" component={OnboardingScreen}/>
          <Stack.Screen name="PastOrderScreen" component={PastOrderScreen}/>
        <Stack.Screen name="Login" component={LoginScreen}
                      options={{headerShown: true,headerBackTitle:'Welcome',
                        headerTitle: '', headerTintColor:'black',headerTransparent:true
                      }}
        />
        <Stack.Screen name="Register" component={RegisterScreen}
                      options={{headerShown: true,headerBackTitle:'Welcome',
                        headerTitle: '', headerTintColor:'black',headerTransparent:true
                      }}/>
        <Stack.Screen name="Dashboard" component={TabNavigationBar}/>
          <Stack.Screen name="TableReservation" component={TableReservationScreen}
                       options={{
                           headerShown: true, headerBackTitle: 'Order',
                           headerTitle: '', headerTransparent: true,
                           headerTintColor: 'black'
                       }}/>
      </Stack.Navigator>
    </NavigationContainer>
      </Provider>
  );
};

export default App;
